// Simple CRUD with Vanilla Javascript

let todos = [];

//function to create todo list
function addTodoList(todo) {
  //Adding new list in the first index
  //   console.log("TODO ", todo);
  todos.unshift(todo);
}

//Display the list that have been created to the todos array
function getTodoList() {
  console.log("Your todo list ", todos);
}

//deleting list
function deleteList(queue_id) {
  const filteredLists = todos.filter(function (todo) {
    // console.log(`item ${queue_id}`, todos);
    return todo.queue_id !== queue_id;
  });
  console.log("==============================");
  console.log("You have deleted 1 item!");
  console.log("==============================");
  //   console.log("remaining list ", filteredList);
  todos = filteredLists;
}

addTodoList({
  Activities: "Swimming",
  Date: "20-3-2022",
  Status: "Unchecked",
  queue_id: 1,
});

addTodoList({
  Activities: "Watering plant at the Backyard",
  Date: "22-3-2022",
  Status: "Checked",
  queue_id: 2,
});

addTodoList({
  Activities: "Cleaning bathtub on Sunday",
  Date: "29-3-2022",
  Status: "Unchecked",
  queue_id: 3,
});

//before
getTodoList();

//after
deleteList(1);

//after
getTodoList();
